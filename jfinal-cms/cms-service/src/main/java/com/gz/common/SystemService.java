package com.gz.common;

import com.gz.common.model.SystemParam;
import com.gz.common.model.Xiaochengxu;

public class SystemService {
    private static SystemService service;

    private SystemService() {
    }

    public static SystemService getService() {
        if (service == null) {
            service = new SystemService();
        }
        return service;
    }
    public Xiaochengxu getXiaochengxu(){
        return Xiaochengxu.dao.findFirst("SELECT * FROM tb_xiaochengxu ");
    }
    public SystemParam getSystem(String name){
        return SystemParam.dao.findFirst("SELECT * FROM tb_system_param WHERE  name=?",name);
    }
}
